
#ifndef SERVEUR_AJOUTS_H
#define SERVEUR_AJOUTS_H

/**
 * Enumération des types de fichiers spécifiques supportés
 */
enum TypeFichier {

    //Texte
    HTML,
    CSS,
    CSV,
    XML,
    SVG,
    PLAIN, //(.txt)
    JSON,
    JAVASCRIPT,

    //Fichiers binaires
    JPEG,
    ICO,
    PNG,
    PDF,

    UNKNOWN // Fichiers d'un autre type
};

/**
 * Taille maximale d'un nom de fichier
 */
#define MAX_NOM_FICHIER 255

/**************************************************
 * GESTION ET TRAITEMENT DES REQUETES
 **************************************************/


/**
 * Regroupe les différentes lignes d'une requête HTTP
 * @return Requête HTTP complète
 */
char* receptionTrameComplete();

/**
 * Renvoie la commande de la requête HTTP
 * @param requete HTTP
 * @return "GET", "POST" ...
 */
char* typeRequete(char *requete);

/**
 * Renvoie le fichier demandé par la requête HTTP
 * @param requete HTTP
 * @param longeurRequete Longueur de la chaine 'requete'
 * @param nomFichier
 * @param maxNomFichier
 * @return 1 si tout c'est bien passé, -1 si la requete est mal formée, -2 si 'nomFichier' est trop petit
 */
int extraitFichier(char* requete, size_t longeurRequete, char* nomFichier, size_t maxNomFichier);



/**************************************************
 * MANIPULATION DES FICHIERS
 **************************************************/

 /**
  * Renvoie le type de fichier grâce à l'enum TypeFichier
  * @param nomFichier
  * @return type : TypeFichier
  */
enum TypeFichier getTypeFichier(char* nomFichier);

 /**
  * Renvoie la longeur du fichier, -1 si le fichier n'existe pas
  * @param nomFichier
  * @return Longueur du fichier
  */
int longeurFichier(char *nomFichier);

 /**
  * Envoie le contenu du fichier au client
  * @param nomFichier
  * @return 1 si l'envoi s'est bien passé, 0 sinon.
  */
int envoyerContenuFichier(char * nomFichier);

/**
 * Envoie le contenu du fichier binaire au client
 * @param nomFichier
 * @return 1 si l'envoi s'est bien passé, 0 sinon.
 */
int envoyerContenuFichierBinaire(char * nomFichier);


/**************************************************
 * EMMISION DES REPONSES HTTP
 **************************************************/

 /**
  * Envoie la réponse HTTP 200 avec le contenu du fichier demandé
  * @param nomFichier
  * @return 1 si l'envoi s'est bien passé, 0 sinon.
  */
int envoyerReponse200(char * nomFichier);

 /**
  * Envoie la réponse correspondante avec une page HTML au client
  * @param noErreur Par exemple 404 pour un fichier introuvable
  * @return 1 si l'envoi s'est bien passé, 0 sinon.
  */
int envoyerReponseErreur(int noErreur);

#endif //SERVEUR_AJOUTS_H
