#include <stdio.h>
#include <string.h>
#include <malloc.h>
#include "serveur.h"
#include "ajouts.h"


int main() {
#ifdef WIN32
	setbuf(stdout, NULL);
#endif

	char nomFichier[MAX_NOM_FICHIER];
	char *message = NULL;

	// On ouvre le socket d'écoute
	Initialisation();

	// Simplement pour que CLion ne crie pas.
    #pragma ide diagnostic ignored "EndlessLoop"

	while(1) {

	    // On attend qu'un client se présente
		AttenteClient();

		// On rassemble la requête HTTP
		message = receptionTrameComplete();

		char* typeReq = typeRequete(message);
		// On filtre les types non connus
		if(strcmp(typeReq, "GET") != 0){
			// Emettre "501 Not Implemented"
            envoyerReponseErreur(501);
            fprintf(stderr,"Requête reçue de type %s\n", typeReq);
		} else {
		    // On récupère le nom de fichier
                switch (extraitFichier(message, strlen(message), nomFichier, MAX_NOM_FICHIER)) {
                    case -1:
                        // Requête mal formée envoi de l'erreur 400
                        envoyerReponseErreur(400);
                        break;

                    case -2:
                        // Nom de fichier trop long envoi de l'erreur 414
                        envoyerReponseErreur(414);
                        break;

                    // Tout s'est bien passé
                    case 1:
                        if (strcmp("", nomFichier) == 0) strcpy(nomFichier, "index.html"); // Si on demande / on envoie index.html

                        // On affiche
                        int contentLength = longeurFichier(nomFichier);
                        printf("Requête pour %s reçu de type %s (taille : %d octets)\n", nomFichier, typeReq, contentLength);
                        if(contentLength == -1){
                            // Emettre 404 file not found
                            envoyerReponseErreur(404);
                            fprintf(stderr, "    Fichier introuvable: %s\n", nomFichier);
                        } else envoyerReponse200(nomFichier);
                        break;

                    // On n'arriveras j'amais içi c'est simplement pour la forme (et eviter que CLion ne crie)
                    default:
                        break;
                }
		}

		free(message);
		free(typeReq);

		// On ferme le socket pour que le navigateur comprenne que la réponse est bien terminée
		TerminaisonClient();
	}
}

