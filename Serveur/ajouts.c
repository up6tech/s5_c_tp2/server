#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include "serveur.h"
#include "ajouts.h"


/**************************************************
 * GESTION ET TRAITEMENT DES REQUETES
 **************************************************/


/**
 * Regroupe les différentes lignes d'une requête HTTP
 * @return Requête HTTP complète
 */
char* receptionTrameComplete() {

    char* requete = calloc(LONGUEUR_TAMPON, sizeof(char));
    bool fini = false;

    do {
        char* morceauCourant = Reception();
        if(morceauCourant == NULL) break;
        if (strcmp(morceauCourant, "\r\n") == 0) fini = true;
        else requete = strcat(requete, morceauCourant);

        free(morceauCourant);
    } while (!fini);

    return requete;
}

/**
 * Renvoie la commande de la requête HTTP
 * @param requete HTTP
 * @return "GET", "POST" ...
 */
char* typeRequete(char *requete) {

    // On coupe au premier espace de la chaine
    int pos = (int) strcspn(requete, " ");

    char* typeReq = calloc(pos +1, sizeof(char));
    strncat(typeReq, requete, pos);

    return typeReq;
}

/**
 * Détermine si le char est un hexadécimal ou non (utilisé par decodeURL() )
 * @param x
 * @return 1 si c'est un hexadécimal
 */
int ishex(int x) {
    return	(x >= '0' && x <= '9')	|| (x >= 'a' && x <= 'f')	|| (x >= 'A' && x <= 'F');
}

/**
 * Décode les caractères spéciaux des URL
 * Volé sur https://rosettacode.org/wiki/URL_decoding#C
 * On ne retrouve pas la signature de cette méthode dans le .h car elle n'est utile qu'ici.
 */
int decodeURL(const char *source, char *dest) {
    char *o;
    const char *end = source + strlen(source);
    int c;

    for (o = dest; source <= end; o++) {
        c = *source++;
        if (c == '+') c = ' ';
        else if (c == '%' && (	!ishex(*source++)	|| !ishex(*source++)	|| !sscanf(source - 2, "%2x", &c))) return -1;
        if (dest) *o = c;
    }
    return o - dest;
}

/**
 * Renvoie le fichier demandé par la requête HTTP
 * @param requete HTTP
 * @param longeurRequete Longueur de la chaine 'requete'
 * @param nomFichier
 * @param maxNomFichier
 * @return 1 si tout s'est bien passé, -1 si la requete est mal formée, -2 si 'nomFichier' est trop petit
 */
int extraitFichier(char* requete, size_t longeurRequete, char* nomFichier, size_t maxNomFichier) {

    // On récupère la commande HTTP pour calculer la position du début du fichier
    char* typeReq = typeRequete(requete);
    size_t lenTypeReq = strlen(typeReq) + 1;

    char* tempNomFichier = calloc(MAX_NOM_FICHIER, sizeof(char));

    // On a la position du dernier caractère du fichier
    signed int positionFinNomFichier = (signed int) strcspn(requete + lenTypeReq, " ");


    if (positionFinNomFichier >= longeurRequete) {
        fprintf(stderr, "Requête mal formée. Nom de fichier introuvable.\n");
        free(typeReq);
        return -1;
    } else if (positionFinNomFichier -2 > (signed int) maxNomFichier) { // -1 pour laisser une place à '\0' et au / de la requete
        fprintf(stderr, "Nom de fichier trop long.\n");
        free(typeReq);
        return -2;
    }

    if (positionFinNomFichier == 1) *tempNomFichier = '\0'; // Sinon strncpy copie 0 char dans la chaine
    // On découpe la chaine pour ne garder que le nom du fichier
    // On ajoute le 1 pour exclure le '/' et on extrait le fichier de la requête
    else strncpy(tempNomFichier, requete + lenTypeReq + 1, positionFinNomFichier -1);

    *(tempNomFichier + positionFinNomFichier -1) = '\0'; // On ajoute le char de fin car strncpy ne le fait pas.

    decodeURL(tempNomFichier, nomFichier);
    free(typeReq);
    free(tempNomFichier);
    return 1;
}



/**************************************************
 * MANIPULATION DES FICHIERS
 **************************************************/

/**
 * Renvoie le type de fichier grâce à l'enum TypeFichier
 * @param nomFichier
 * @return type : TypeFichier
 */
enum TypeFichier getTypeFichier(char* nomFichier) {

    char* extension = strrchr(nomFichier, '.');

    // C'est un peu moche mais je n'ai pas eu d'idée plus efficace etant donné
    // qu'on ne peut utiliser un switch pour comparer des chaines. DG.
    if (strcmp(extension, ".html") == 0) return HTML;
    else if (strcmp(extension, ".css") == 0) return CSS;
    else if (strcmp(extension, ".csv") == 0) return CSV;
    else if (strcmp(extension, ".xml") == 0) return XML;
    else if (strcmp(extension, ".svg") == 0) return SVG;
    else if (strcmp(extension, ".txt") == 0) return PLAIN;
    else if (strcmp(extension, ".json") == 0) return JSON;
    else if (strcmp(extension, ".js") == 0) return JAVASCRIPT;
    else if (strcmp(extension, ".ico") == 0) return ICO;
    else if (strcmp(extension, ".jpg") == 0 || strcmp(extension, ".jpeg") == 0) return JPEG;
    else if (strcmp(extension, ".png") == 0) return PNG;
    else if (strcmp(extension, ".pdf") == 0) return PDF;
    else return UNKNOWN;
}

/**
 * Renvoie la longeur du fichier, -1 si le fichier n'existe pas
 * @param nomFichier
 * @return Longueur du fichier
 */
int longeurFichier(char *nomFichier) {
    FILE *file = NULL;
    file = fopen(nomFichier, "r");

    if (file == NULL) {
        return -1;
    } else {
        fseek(file, 0, SEEK_END);
        size_t tailleFichier = ftell(file);
        fclose(file);
        return (int) tailleFichier;
    }
}

/**
 * Envoie le contenu du fichier au client
 * @param nomFichier
 * @return 1 si l'envoi s'est bien passé, 0 sinon.
 */
int envoyerContenuFichier(char * nomFichier){
    FILE *file = NULL;
    file = fopen(nomFichier, "r");
    if(file == NULL) return 0;

    const int maxLen = longeurFichier(nomFichier) +1;
    char line[maxLen];
    char* pLine = NULL;

    while((pLine = fgets(line, maxLen, file)) != NULL){

        // Si pLine ne termine pas par un '\n'
        if (strstr(pLine, "\n") == NULL) {
            strcat(pLine, "\n");
        }
        if (!Emission(pLine)) {
            free(pLine);
            return 0;
        }
    }

    free(pLine);
    return 1;
}

/**
 * Envoie le contenu du fichier binaire au client
 * @param nomFichier
 * @return 1 si l'envoi s'est bien passé, 0 sinon.
 */
int envoyerContenuFichierBinaire(char * nomFichier){
    FILE *file = NULL;
    file = fopen(nomFichier, "rb");
    if(file == NULL) return 0;

    char* binaryFile = calloc(longeurFichier(nomFichier), sizeof(char));

    fread(binaryFile, longeurFichier(nomFichier) * sizeof(char), 1, file);


    if (!EmissionBinaire(binaryFile, longeurFichier(nomFichier))) {
        fclose(file);
        free(binaryFile);
        return 0;
    }

    fclose(file);
    free(binaryFile);
    return 1;
}


/**************************************************
 * EMMISION DES REPONSES HTTP
 **************************************************/

/**
 * Envoie la réponse HTTP 200 avec le contenu du fichier demandé
 * @param nomFichier
 * @return 1 si l'envoi s'est bien passé, 0 sinon.
 */
int envoyerReponse200(char * nomFichier){

    enum TypeFichier type = getTypeFichier(nomFichier);
    char contentType[50];
    bool isText;

    switch (type) {

        case HTML:
            isText = 1;
            strcpy(contentType, "text/html");
            break;
        case CSS:
            isText = 1;
            strcpy(contentType, "text/css");
            break;
        case CSV:
            isText = 1;
            strcpy(contentType, "text/csv");
            break;
        case XML:
            isText = 1;
            strcpy(contentType, "text/xml");
            break;
        case SVG:
            isText = 1;
            strcpy(contentType, "image/svg+xml");
            break;
        case PLAIN:
            isText = 1;
            strcpy(contentType, "text/plain");
            break;
        case JSON:
            isText = 1;
            strcpy(contentType, "application/json");
            break;
        case JAVASCRIPT:
            isText = 1;
            strcpy(contentType, "application/javascript");
            break;
        case JPEG:
            isText = 0;
            strcpy(contentType, "image/jpeg");
            break;
        case ICO:
            isText = 0;
            strcpy(contentType, "image/x-icon");
            break;
        case PNG:
            isText = 0;
            strcpy(contentType, "image/png");
            break;
        case PDF:
            isText = 0;
            strcpy(contentType, "application/pdf");
            break;
        case UNKNOWN:
        default: // Couvre le UNKNOWN et les valeurs incorrectes normalement impossibles
            isText = 0;
            strcpy(contentType, "application/octet-stream"); // Type générique. Déclenche le plus souvent un téléchargement sur le poste du client
            break;
    }

    // Préparation et envoi de l'en-tête
    char header[150];
    sprintf(header, "HTTP/1.1 200 OK\r\nServer: STRI CAYRE GARDES\r\nContent-type: %s\r\nContent-length: %d\r\n\r\n", contentType, (int) longeurFichier(nomFichier));
    if (!Emission(header)) return 0;

    // On envoie le contenu du fichier
    if (isText) {
        if (!envoyerContenuFichier(nomFichier)) return 0;
    } else {
        if (!envoyerContenuFichierBinaire(nomFichier)) return 0;
    }

    return 1;
}

/**
 * Envoie la réponse correspondante avec une page HTML au client
 * @param noErreur Par exemple 404 pour un fichier introuvable
 * @return 1 si l'envoi s'est bien passé, 0 sinon.
 */
int envoyerReponseErreur(int noErreur){

    char enTete[50];
    char message[50];

    switch (noErreur) {
        case 400:
            strcpy(enTete, "400 Bad Request");
            strcpy(message, "Erreur requête mal formée.");
            break;

        case 404:
            strcpy(enTete, "404 Not Found");
            strcpy(message, "Erreur fichier introuvable.");
            break;

        case 414:
            strcpy(enTete, "414 URI Too Long");
            strcpy(message, "Erreur URI trop longue.");
            break;

        case 501:
            strcpy(enTete, "501 Not Implemented");
            strcpy(message, "Erreur type de requête non supportée.");
            break;

        default:
            strcpy(enTete, "500 Server Error");
            strcpy(message, "Erreur serveur.");
            noErreur = 500;
            break;
    }

    // Le contenu de la page d'erreur est directement "en dur" dans le code pour l'avoir à disposition quoi qu'il arrive
    char htmlPage[500];
    sprintf(htmlPage, "<!DOCTYPE html>\n<html lang=\"fr\">\n<head>\n<meta charset=\"UTF-8\">\n<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n<title>Erreur %d</title>\n</head>\n<body>\n<h1>%s</h1>\n</body>\n</html>\n", noErreur, message);

    char header[150];
    sprintf(header, "HTTP/1.1 %s\r\nServer: STRI CAYRE GARDES\r\nContent-type: text/html\r\nContent-length: %d\r\n\r\n", enTete, (int) strlen(htmlPage));

    if (!Emission(header)) return 0;
    if (!Emission(htmlPage)) return 0;

    return 1;
}
